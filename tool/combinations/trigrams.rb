#!/bin/env ruby
# Generate the list of numbers in the Zog numerical system along classical
# decimal and hexadecimal countpart
require 'prime'

polysemous = %w{
  bal bar bas bat bay beh ber bes bin bir bis biy bon can car cat cec
  cel cen cet cil cin cus cuz dah dal das deg deh dek den des dev dew
  dez dic din dis dit div diw dix dol dos duj dul duo dus fem fir fol
  guc hat hen hex hin hok hor hos hot hun jat jav jek jer jet jol joz
  jun juz kat kay kil kin kuz kyz lak lam lim loS luk mal mek men mil
  min mot mun nam nas nau nav naw nef neh nen net nev nin nis nod noh
  nol non not nov noy nuf nul nus pal peb pes pet pin qir sab sad sam
  san sap sas sat sax saṭ sed sei sel sem sen sep ser ses set sex sez
  sih sim sin sip sis sit six sol sot sov syv tam tan tas tav tel ten
  ter tin tis tiz tlon tos tum vel ven vis vos vot vyf wan wax wej wit
  yek yin yod yon yot yoz yuk yun yuz zar zes zul zun
}

consons = %w{b d f k l m n r s t v z}.reverse
vowels = %w{u a e i o}

# Provides an enumerable nomenclature, generating all combination not present
# in +outcast+ which are of the form "EME", with E taken in +extremes+, and
# M in +middle+.
def nomenclature(extremes, middle, outcast)
  format = ->(ordinal, name) do
    primality = %w{ḃ ḋ ḟ k̇ l̇ ṁ ṅ ṙ ṡ ṫ v̇ ż}.reverse
    name[2] = primality[extremes.index(name[2])] if ordinal.prime?
    # Matches congruency for 2, 8, 12, 16 using a binary flag sequence
    #
    # Note: for some reason, `tr!` won’t return the proper result for
    # some diacritics `sub!` was used instead in these cases.
    # Otherwise name.tr!('aeiou', marks[flags]) unless flags = '0000'
    # would have been possible.
    #
    # Correspondance of diacritics with multiplicity goes like this:
    # - caron: 60
    # - macron: 20
    # - cicumflex: 16 and 12
    # - grave: 16
    # - acute: 12
    # - ogonek: 8
    # - trema: 2 (only alone)
    case [2, 8, 12, 16, 20, 60].map{|n| (ordinal % n) == 0 ? '1' : '0' }.join
    when /.1.1.1/ # multiple of 8, 16 and 60  (0, 240, 480…)
      name.sub!(/[aeiou]/, 'a' => 'ą̀̌', 'e' => 'ę̀̌', 'i' => 'į̀̌', 'o' => 'ǫ̀̌', 'u' => 'ų̀̌')
    when /.1.0.1/ # multiple of 8 and 60 but *not* 16 (120, 360, 600…)
      name.sub!(/[aeiou]/, 'a' => 'ą̌', 'e' => 'ę̌', 'i' => 'į̌', 'o' => 'ǫ̌', 'u' => 'ų̌')
    when /.0.0.1/ # any other multiple of 60 is also one of 2, 12 and 20
      name.tr!('aeiou', 'ǎěǐǒǔ')
    when '111110'
      name.sub!(/[aeiou]/, 'a' => 'ą̄̂', 'e' => 'ę̄̂', 'i' => 'į̄̂', 'o' => 'ǭ̂', 'u' => 'ų̄̂')
    when '111100'
      name.sub!(/[aeiou]/, 'a' => 'ą̂', 'e' => 'ę̂', 'i' => 'į̂', 'o' => 'ǫ̂', 'u' => 'ų̂')
    when '111010'
      name.sub!(/[aeiou]/, 'a' => 'ą̄́', 'e' => 'ę̄́', 'i' => 'į̄́', 'o' => 'ǭ́', 'u' => 'ų̄́')
    when '111000'
      name.sub!(/[aeiou]/, 'a' => 'ą́', 'e' => 'ę́', 'i' => 'į̇́', 'o' => 'ǫ́', 'u' => 'ų́')
    when '110110'
      name.sub!(/[aeiou]/, 'a' => 'ą̄̀', 'e' => 'ę̄̀', 'i' => 'į̄̀', 'o' => 'ǭ̀', 'u' => 'ų̄̀')
    when '110100'
      name.sub!(/[aeiou]/, 'a' => 'ą̀', 'e' => 'ę̀', 'i' => 'į̀', 'o' => 'ǫ̀', 'u' => 'ų̀')
    when '101100'
      name.tr!('aeiou', 'âêîôû')
    when '110010'
      name.sub!(/[aeiou]/, 'a' => 'ą̄', 'e' => 'ę̄', 'i' => 'į̄', 'o' => 'ǭ', 'u' => 'ų̄')
    when '100010'
      name.tr!('aeiou', 'āēīōū')
    when '100100'
      name.tr!('aeiou', 'àèìòù')
    when '101000'
      name.tr!('aeiou', 'áéíóú')
    when '110000'
      name.tr!('aeiou', 'ąęįǫų')
    when '100000'
      name.tr!('aeiou', 'äëïöü')
    end
    "#{sprintf('%03d', ordinal)} (#{sprintf('%02X', ordinal)}):  #{name}"
  end

  Enumerator.new do |yielder|
    ordinal = 0
    extremes.each do |aft|
      extremes.each do |bow|
        terms = (0..(middle.length-1)).map{|i| bow + middle[i] + aft}
        next if terms.any?{|term| outcast.member?(term)}

        if (ordinal % 5 == 0)
          yielder.yield format[ordinal, terms.last]
          ordinal +=1
        end
        (0..(middle.length-2)).reverse_each do |bib|
          yielder.yield format[ordinal, terms[bib]]
          ordinal += 1
        end
      end
    end
  end
end

nomenclature(consons, vowels, polysemous).each{|line| puts line}
