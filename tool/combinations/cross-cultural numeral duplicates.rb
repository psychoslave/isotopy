#!/bin/env ruby
# Compute the list of colliding terms among a subset of number names accross
# different traditions. The considered terms are only those being syllabs with
# a VCV form.
#
# For more background on this, see https://w.wiki/S7o
units = {
  '0': %w{nol nul ser},
  '1': %w{bat bal ber bir cyp din hin hun hõs jat jun jek ków mek men một sad sas sis vun yan yek yin yun wan},
  '2': %w{dew div dos dós dol duj dul dúo dus duy kil nis pud sam sen sin tel tan tin},
  '3': %w{kay kil ke̮m nas peb san sān sam set tin wej},
  '4': %w{bốn car cār çar can fir fol kat kuz kuẓ loS mus net ne’w wax yon ŋas ŋús},
  '5': %w{bäş bes beş beš biš dỉt dỉw fem füf kin lăm lim năm na’n pâtʹ p’âtʹ peş pet pět sen tlón vad vīž vyf},
  '6': %w{fut hat héx hòk jav kūž kuz’ líx luk mer nam sas sax sei ses ṣaṣ šaš şeş sex sis sîs šiš šit six śov šov yuk zes},
  '7': %w{bảy cat jèt šab sāt sân sat sât semʹ sém sep set sét sèt sim sìm syv vel yot},
  '8': %w{bat cec hen jöl pal sam ses tam vos vot wit},
  '9': %w{dés dev hìn naŭ nav naw neh nen nèf nëf neh név nin noh non nov nóv noy nûf tiš tos zül},
  '10': %w{can dah das deg deh dek des deś deš dés dez dic dis dîs dix doc sap sìp tan tán tas ten tin tíz zäh},
  'A₁₂': %w{mal},
  '11': %w{},
  'B₁₂': %w{tam},
  '12': %w{bar bār},
  '13': %w{ter tér},
  '14': %w{},
  '15': %w{},
  '16': %w{sèz soḷ sōl},
  '17': %w{},
  '18': %w{},
  '19': %w{},
  '20': %w{bin biš bîs bīs horʹ hot kyzʹ qad )ven vis},
  '30': %w{guč sîh tīs tîs},
  '40': %w{čel çil qir},
  '50': %w{sén tavʹ},
  '60': %w{ṣaṭ žar},
  '70': %w{dal},
  '80': %w{sát},
  '90': %w{jer not nod},
  '100': %w{beh bɨy cem cen cét çüs cüz dèŋ jôẓ jüz sad san sáñ sat sed šel šêl sël sem śĕr śôt tum yöð yöz yüz ýüz zun},
  '1000': %w{bin biñ cin kún mil min mıñ muñ müň pin sen ših ter},
  '100000': %w{lak},
}

art = {
  '1': %w{din vek men mus wun ven ren nit von zab mîn din bal won},
  '2': %w{kan dut sam dus dov sop fic zár dof dul zac kör tâd gel zay tel wer},
  '3': %w{ter taʃ sed zaw mín dol höp wej tin zad nêl dil kil yām},
  '4': %w{fin kut far boθ tor hir got loS bim car zaf vol fol ber},
  '5': %w{faf kel kud vat mek hen kin pet lim zag pen mul fem pin lul},
  '6': %w{luk zix gab peŋ sex sok ses sex sis six zot pek zal fal mäl},
  '7': %w{mon yep dem zos sen sep ros sem zam zun tel zaf vel},
  '10₈': %w{vol},
  '8': %w{bar lok got nib bal zaq vos kil waf jöl},
  '9': %w{nit val nif hez nen naw naŭ nov non nun nin nof Hut bud nef zar dev pol saf zül},
  '10': %w{nen dif men dek des ten mák des hal deg},
  'A₁₂': %w{mal},
  'B₁₂': %w{tam},
  '11': %w{mir b̃ȧb̃ len},
  '12': %w{dir b̃ȧd̃},
  '13': %w{tir b̃ȧf̃},
  '14': %w{kir b̃ȧg̃},
  '15': %w{b̃ȧh̃},
  '16': %w{b̃ȧj̃},
  '17': %w{b̃ȧk̃},
  '18': %w{b̃ȧl̃},
  '19': %w{b̃ȧm̃},
  '50': %w{sén},
  '100₈': %w{zam},
  '80': %w{sát},
  '100': %w{tan son ken ben cen gār ňal hon zaw hel tum teh},
  '1000': %w{tan mel mil kil zay hil},
  '10000': %w{hol},
  '100000': %w{mak bip hul},
  '1000000': %w{dur yor rod},
}

class String
  def diacriticless
    self.tr( "ÀÁÂÃÄÅàáâãäåĀāĂăĄąÇçĆćĈĉĊċČčÐðĎďĐđÈÉÊËèéêëĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħÌÍÎÏìíîïĨĩĪīĬĭĮįİıĴĵĶķĸĹĺĻļĽľĿŀŁłÑñŃńŅņŇňŉŊŋÒÓÔÕÖØòóôõöøŌōŎŏŐőŔŕŖŗŘřŚśŜŝŞşŠšȘșſŢţŤťŦŧȚțÙÚÛÜùúûüŨũŪūŬŭŮůŰűŲųŴŵÝýÿŶŷŸŹźŻżŽžộẓỉỉṣảḷɨ", "AAAAAAaaaaaaAaAaAaCcCcCcCcCcDdDdDdEEEEeeeeEeEeEeEeEeGgGgGgGgHhHhIIIIiiiiIiIiIiIiIiJjKkkLlLlLlLlLlNnNnNnNnnNnOOOOOOooooooOoOoOoRrRrRrSsSsSsSsSssTtTtTtTtUUUUuuuuUuUuUuUuUuUuWwYyyYyYZzZzZzoziisali").sub("ʹ", '').sub("ʹ", '')
  end
end

units.each{|term, values| units[term] = values.union( (art[term] || []) ) }

# Build the hash of duplicates, grouped by name.
# Note: all but optimal, but straight forward algorithm.
duplicates = Hash.new
flat_duplicates = Hash.new
units.each do |main, words|
  units.each do |other, terms|
    next if main == other
    next if words.nil?
    words.each do |word|
      sleek = word.diacriticless
      next unless terms.include?(word) || terms.include?(sleek)
      duplicates[word] = (duplicates[word] || []).union [main, other]
      flat_duplicates[sleek] = (duplicates[sleek] || []).union [main, other]
    end
  end
end

# Display each colliding term with the set of corresponding meanings
duplicates.each do |name, values|
  puts "#{name}: #{values.join("\t")}"
end

# The same, but without taking account of diacritics
puts '-' * 80
flat_duplicates.each do |name, values|
  puts "#{name}: #{values.sort.join("\t")}"
end

